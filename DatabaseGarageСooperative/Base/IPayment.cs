﻿using System;
using System.Data;

namespace DatabaseGarageСooperative.Base
{
    public interface IPayment: IEnity
    {
        /// <summary>
        /// Дневной платёж
        /// </summary>
        int MonthlyPayment { set; get; }

        DateTime DateTime { set; get; }
    }
}