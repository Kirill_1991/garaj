﻿using System;
using System.Data;

namespace DatabaseGarageСooperative.Base
{
    public class Payment : EntityBase, IPayment
    {
        /// <summary>
        /// Месячный платёж
        /// </summary>
        public int MonthlyPayment { get; set; }
        /// <summary>
        /// Дата изменения платежа
        /// </summary>
        public DateTime DateTime { get; set; }
        //public DataTable DateСhange { get; set; }

        public int? GarageId { set; get; }

        public virtual Garage CurrentGarage { set; get; }
       
    }
}