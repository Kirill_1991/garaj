﻿using System.Collections.Generic;

namespace DatabaseGarageСooperative.Base
{
    public class Garage : EntityBase, IGarage
    {
        public string NumGarage { get; set; }

        public virtual Person Person { set; get; }

        public ICollection<Payment> CurrentPayments { set; get; }
    }
}