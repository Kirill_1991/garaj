﻿namespace DatabaseGarageСooperative.Base
{
    public interface IPerson:IEnity
    {
        /// <summary>
        /// Имя
        /// </summary>
        string Name { set; get; }

        /// <summary>
        /// Фамилия
        /// </summary>
        string Surname { set; get; }

        /// <summary>
        /// Oтчество
        /// </summary>
        string Patronymic { get; set; }
    }
}