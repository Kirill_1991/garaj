﻿using DatabaseGarageСooperative.Base;

namespace DatabaseGarageСooperative
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class Context : DbContext
    {
        public Context()
            : base("name=Context")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<Context, Migrations.Configuration>());
            Configuration.LazyLoadingEnabled = false;//раскаментить
        }

        public DbSet<Person> Persons { set; get; }

        public DbSet<Garage> Garages { set; get; }

        public DbSet<Payment> Payments { set; get; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
           
            modelBuilder.Entity<Person>()
                .HasOptional(s => s.Garage) 
                .WithRequired(ad => ad.Person);

            modelBuilder.Entity<Payment>()
                .HasOptional<Garage>(f => f.CurrentGarage)
                .WithMany(gr => gr.CurrentPayments)
                .HasForeignKey(i => i.GarageId);
        }
    }
}