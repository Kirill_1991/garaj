﻿namespace DatabaseGarageСooperative.Base
{
    public enum Roles
    {
        /// <summary>
        /// Председатель
        /// </summary>
        Chairperson,
        /// <summary>
        /// охранник
        /// </summary>
        Guard,
        /// <summary>
        /// Хозяин гаража
        /// </summary>
        Owner,
    }
    public class Person : EntityBase, IPerson
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public string Patronymic { get; set; }

        public string Login { set; get; }

        public string Password { set; get; }

        public string Telefonnummer { set; get; }

        public Roles Roles { set; get; }

        public virtual Garage Garage { set; get; }

    }


}